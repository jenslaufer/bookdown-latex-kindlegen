FROM r-base:4.0.2

RUN wget http://kindlegen.s3.amazonaws.com/kindlegen_linux_2.6_i386_v2_9.tar.gz -O /tmp/kindlegen_linux_2.6_i386_v2_9.tar.gz
RUN tar -xzf /tmp/kindlegen_linux_2.6_i386_v2_9.tar.gz -C /tmp
RUN mv /tmp/kindlegen /usr/bin

RUN apt-get update && apt-get install -y libcurl4-gnutls-dev libxml2-dev libssl-dev

RUN R -e "install.packages('tidyverse')"
RUN R -e "install.packages('tinytex')"
RUN R -e "tinytex::install_tinytex()"
RUN R -e "install.packages('glue')"
RUN R -e "install.packages('bookdown')"
RUN R -e "install.packages('devtools')"